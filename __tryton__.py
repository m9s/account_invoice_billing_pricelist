#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Billing Pricelist',
    'name_de_DE': 'Fakturierung aus Abrechnungsdaten mit Preislisten',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Billing Pricelist
    - Allow the use of pricelists with Account Invoice Billing:
    Use pricelists when creating invoices directly from billing lines listed
    in a separate table.
''',
    'description_de_DE': '''Rechnungsstellung aus Abrechnungsdaten mit Preislisten
    - Ermöglicht die Generierung von Rechnungen auf Basis von in einer
      Tabelle erfassten Abrechnungsdaten unter Verwendung von Preislisten.
''',
    'depends': [
        'account_invoice_billing',
        'account_invoice_pricelist',
    ],
    'xml': [
        'billing.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
