#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.transaction import Transaction
from trytond.pool import Pool


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    def __init__(self):
        super(BillingLine, self).__init__()
        self._error_messages.update({
            'no_sale_pricelist': 'Party %s (%s) must have a sale price list!',
            'no_price_for_date': 'Can not find a price for product %s at %s!',
            })

    def _create_invoice(self, invoice_values, billing_line):
        party_obj = Pool().get('party.party')

        pricelist = self._get_pricelist_from_line(billing_line)
        if not pricelist:
            party = party_obj.browse(invoice_values['party'])
            self.raise_user_error('no_sale_pricelist',
                (party.rec_name, party.code))
        invoice_values.setdefault('pricelist', pricelist.id)

        return super(BillingLine, self)._create_invoice(invoice_values,
            billing_line)

    def _get_pricelist_from_line(self, line):
        if line.party.pricelist_sale:
            return line.party.pricelist_sale
        return False

    def _get_unit_price_from_line(self, line):
        pricelist_obj = Pool().get('pricelist.pricelist')
        user_obj = Pool().get('res.user')

        context = Transaction().context
        if line.company.currency:
            context['currency'] = line.company.currency.id

        pricelist = self._get_pricelist_from_line(line)
        if pricelist:
            context['pricelist'] = pricelist.id

        if line.billing_date:
            context['price_date'] = line.billing_date
            context['date'] = line.billing_date

        with Transaction().set_context(**context):
            unit_price = pricelist_obj.get_price([line.product.id],
                quantity=line.quantity)[line.product.id]
        if not unit_price:
            user = user_obj.browse(Transaction().user)
            lang = user.language
            self.raise_user_error(
                'no_price_for_date', error_args=(line.product.name,
                line.billing_date.strftime(str(lang.date))))
        return unit_price

BillingLine()

class CreateInvoicesAskPricelist(ModelView):
    'Create Invoices Ask Pricelist'
    _name = 'account.invoice.billing_line.create_invoices.ask_pricelist'
    _description = __doc__
    party = fields.Many2One('party.party', 'Customer', readonly=True)
    company = fields.Many2One('company.company', 'Company', readonly=True)
    pricelist_sale = fields.Many2One(
        'pricelist.pricelist', 'Pricelist', required=True)

CreateInvoicesAskPricelist()


class CreateInvoices(Wizard):
    _name = 'account.invoice.billing_line.create_invoices'

    def __init__(self):
        super(CreateInvoices, self).__init__()
        self.states = copy.copy(self.states)
        if 'ask_pricelist' not in self.states:
            self.states['ask_pricelist'] = {
               'actions': ['_set_default_pricelist_sale'],
               'result': {
                   'type': 'form',
                   'object': 'account.invoice.billing_line.create_invoices.' \
                             'ask_pricelist',
                   'state': [
                       ('end', 'Cancel', 'tryton-cancel'),
                       ('init', 'Continue', 'tryton-go-next', True),
                       ],
                   },
               }

    def _set_default_pricelist_sale(self, data):
        billing_line_obj = Pool().get('account.invoice.billing_line')

        line_ids = billing_line_obj.search([('invoice_line', '=', False)])
        billing_lines = billing_line_obj.browse(line_ids)

        for line in billing_lines:
            if line.party.pricelist_sale:
                continue
            if not line.party.pricelist_sale:
                return {'party': line.party.id,'company': line.company.id}

        return {'party': line.party.id,'company': line.company.id}

    def _create_invoices(self, data):
        billing_line_obj = Pool().get('account.invoice.billing_line')
        party_obj = Pool().get('party.party')

        form = data['form']

        if form.get('pricelist_sale') and form.get('party') and \
                    form.get('company'):
            with Transaction().set_context(company=form['company']):
                result = party_obj.write(form['party'], {
                        'pricelist_sale': form['pricelist_sale']})

        line_ids = billing_line_obj.search([('invoice_line', '=', False)])

        billing_lines = billing_line_obj.browse(line_ids)
        for line in billing_lines:
            if not line.party.pricelist_sale:
                return 'ask_pricelist'

        res = super(CreateInvoices, self)._create_invoices(data)
        return res

CreateInvoices()
